<?php
include 'conexion.php';
session_start();
if(!isset($_SESSION['user'])){
  $_SESSION['user']=0;
}
$merror="";
$pass="";
$nomb="";
if (isset($_POST["enviar"])) {
  $pass = $_POST["pass"];
  $nomb = $_POST["nomb"];
  $resultado = $conexion->query('SELECT * FROM usuario WHERE nombre="'.$nomb.'"');
   if($resultado->num_rows!=0) {
    while($dis = $resultado->fetch_assoc()){
     if($pass==$dis['Password']){
      $_SESSION['user']=1;
      $_SESSION['nomb']=$nomb;
      $_SESSION['mensaje']=$dis;
      $_SESSION['id']=$dis['IDusuario'];
      //$_SESSION['mensaje']=password_hash($pass,PASSWORD_DEFAULT);
     }else
     $merror="Contraseña incorrecta";
     $_SESSION['mensaje']=$dis;
    }
    }else
    $merror="Nombre de usuario o contraseña incorrecto";
  }
if (isset($_REQUEST['log'])){
  session_destroy();
  header('Location:index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Home</title>
<style type="text/css">
.page{
  margin-left:10px;
}
</style>
 <?php
 include 'header.php';
 ?>
 </head>
 <body>
   <div class="page">
     <div class="row"></div>
  <div class="row">
  <div class="col-md-9">
     <div class="jumbotron">
          <div class="col-md-12">
            <h3 class="h1 text-center">Adopta tu perro en Madrid</h3><br/>
          </div>
          <div class="col-md-8 float-right">
            <img class="img-fluid rounded" src="<?=$url?>img/index.png"/>
          </div>
          <div class="col-md-4">
            <p class="lead">La única aplicacion de busqueda de perros en adopción en la Comunidad de
            Madrid</p>
            <hr class="my-3">
            <p> <!--<?= $_SESSION['id']?>-->  El abandono de perros es una realidad y juntos podemos combatirlo. Este año, nuevamente, 
            más de 80.000 perros han sido recogidos por protectoras de animales en toda España.
            Por eso, desde la plataforma MeetPet nos comprometemos a ayudar a encontrar de una forma sencilla y rápida a tu perro ideal,s
            que se encuentre en algún refugio o perrera de la Comunidad de Madrid.
            </p>
          </div>
      </div>
  </div>
<?php
if($_SESSION['user']==0){
?>
  <div class="col-md-3">
    <h3 class="text-primary">Iniciar Sesión</h3>
    <br/><br/>
    <h4 class="text-danger"><?php echo($merror)?></h4>
    <br/>
    <form class="form" method="post" action="<?=$url?>index.php">
      <div class="form-group">
        <label>Nombre de Usuario</label>
        <input type="text" class="form-control" name="nomb" placeholder="Nombre">
      </div>
      <div class="form-group">
        <label>Contraseña</label>
        <input type="password" class="form-control" name="pass" placeholder="Contraseña">
      </div>
      <button type="submit" class="btn btn-primary" name="enviar">Enviar</button>
     </form>
  </div>
  <?php
    }else{ 
  ?>
<div class="col-md-3">
  <h3 class="text-primary">Bienvenido <?php echo $_SESSION['nomb']?>!!</h3>
  <br/>
  <a class="btn btn-primary" href="<?=$url?>altaPerro.php" role="button">Dar de alta un perro</a>
  <br/><br/>
  <img class="rounded" src="<?=$url?>img/perrobrazos.png"/>
  <br/><br/>
  <a class="btn btn-danger" href="<?=$url?>index.php?log=out" role="button">Cerrar Sesión</a>
</div>
  <?php
    }
  ?>
  </div>
 </body>
</html>