<?php
session_start();
include 'conexion.php';
$nombre="";
$raza="";
$tamaño="";
$sexo="";
$edad="";
$merror="";
$mensaje="";
if(isset($_POST['enviar'])){
    $nombre=$_POST['nombre'];
    $raza=$_POST['raza'];
    $tamaño=$_POST['tamaño'];
    $sexo=$_POST['sexo'];
    $edad=$_POST['edad'];
    $nombref = $_FILES['imagen']['name'];
    $nombrer = strtolower($nombref);
    $cd=$_FILES['imagen']['tmp_name'];
    $ruta = "img/". $_FILES['imagen']['name'];
    $destino = "img/".$nombrer;
    $resultado = @move_uploaded_file($_FILES["imagen"]["tmp_name"], $ruta);

    if($nombre=="" || $raza=="" || $tamaño=="Selecciona" || $sexo=="Selecciona" || $edad==""){
        $merror="No se ha introducido alguno de los campos";
    }else{
        if($tamaño=="1"){
            $tamaño="Pequeño";
        }elseif($tamaño=="2"){
            $tamaño="Mediano";
        }else{
            $tamaño="Grande";
        }
        if($sexo=="1"){
            $sexo="Hembra";
        }else{
            $sexo="Macho";
        }
        

    if (!empty($resultado)){
            $resultado = $conexion->query('INSERT INTO perro (Nombre,Raza,Tamaño,Sexo,Edad,Reservado,IDperrera,Img,Habilitado,Contacto) 
            VALUES ("'.$nombre.'","'.$raza.'","'.$tamaño.'","'.$sexo.'","'.$edad.'","0","4","'.$nombrer.'","0",'.$_SESSION['id'].')'); 
                if(isset($_SESSION['admin'])){   
                $mensaje="Se ha enviado la solicitud: $nombre, $raza, $tamaño, $sexo, $edad años. Muchas gracias";
                header('Location: admin.php?dog=true');
            }else{
                $mensaje="Se ha enviado la solicitud: $nombre, $raza, $tamaño, $sexo, $edad años. Muchas gracias";
                
                }
            }
       

    }
}
?>
<!DOCTYPE html>
<html>
<head>
<title>AltaPerro</title>
<style type="text/css">
</style>
<link rel=StyleSheet href="" type="text/css" media=screen>
 <?php
 include 'header.php';
 ?>
</head>
<body>
<div class="container">
<div class="row">

        <div class="col-md-15">
        <?php
        if (isset($_REQUEST['go'])){
            if(isset($_SESSION['admin'])){?>
        <form method="post" action="<?=$url?>altaPerro.php?go=true&admin=si" enctype="multipart/form-data">
        <?php
        }else{?>
        <form method="post" action="<?=$url?>altaPerro.php?go=true" enctype="multipart/form-data">
       <?php }
        ?>
            <h2 class="text-primary">Alta Perro:</h2><br/>
      
        <div class="form-row">
            <div class="col form-group">
                <label for="exampleInputPassword1">Nombre del Perro</label>
                <input type="text" class="form-control" name="nombre">
            </div>
            <div class="col form-group">
                <label for="exampleInputEmail1">Raza</label>
                <input type="text" class="form-control" name="raza">
            </div>
        </div>
       
        <div class="form-row">
        
            <div class="col form-group">
                <label for="exampleInputPassword1">Tamaño</label>
                <select class="custom-select mr-sm-2" name="tamaño">
                    <option selected>Selecciona</option>
                    <option value="1">Pequeño</option>
                    <option value="2">Mediano</option>
                    <option value="3">Grande</option>
                </select>
            </div>
            <div class="col form-group">
                <label for="exampleInputPassword1">Sexo</label>
                <select class="custom-select mr-sm-2" name="sexo">
                    <option selected>Selecciona</option>
                    <option value="1">Hembra</option>
                    <option value="2">Macho</option>
                </select>
            </div>
        </div>
        
        <div class="form-row">
        <div class="col-md-5">
            <div class=" form-group">
                <label for="exampleInputPassword1">Edad</label>
                <input type="text" class="form-control" name="edad">
            </div>
            
            </div>
        </div>  
        <div class="form-row">
        <div class="col form-group">
        
                <label for="exampleFormControlFile1">Imagen</label>
                <input type="file" accept=" image/*" class="form-control-file" name="imagen">
                <small id="tlfHelp" class="form-text text-muted">Nos harías un favor si guardases el nombre de la imagen del mismo modo que el nombre del perro</small>
            </div>
           
        </div>
            <h3 class="text-danger"><?=$merror?><br/></h3>
            <h3 class="text-success"><?=$mensaje?><br/></h3>
        <div class="row">
        <div class="col-md-6">
        <?php
        if(isset($_REQUEST['admin'])){
            $_SESSION['loged']="si";
            $_SESSION['admin']="si";
            ?>  
            <a class="btn btn-danger" href="<?=$url?>admin.php?dog=true" role="button">Volver</a>
        <?php }?>
        
        <button type="submit" class="btn btn-primary" name="enviar">Enviar</button>
        </div>
       </div>
        </form>
       <?php }else{?>

            <div class="jumbotron">
                <h1 class="display-4">Dar de alta un perro</h1>
                <p class="lead">¿No puedes ocuparte de tu perro? ¿No tienes tiempo? </p>
                <hr class="my-4">
                <p>Si esta dispuesto a darlo en adopción necesitaremos sus datos y una imagen.<br/> 
                Una vez que un administrador lo aprueba, ya estará listo para ser dado en adopcion</p>
                <p class="lead">
                <a class="btn btn-primary btn-lg" href="<?=$url?>altaPerro.php?go=true" role="button">Adelante!</a>
                </p>
            </div>
        <?php }?>
        </div>
    </div>
</div>
</div>
</body>
</html>