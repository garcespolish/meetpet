<?php $url="http://".$_SERVER['SERVER_NAME']."/meetpet/";?>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <style type="text/css">
	.menu{
		font-size:20px;
	}
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="./css/bootstrap.css"/>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="<?=$url?>js/bootstrap.min.js"></script>
    <script src="<?=$url?>js/main.js"></script>
	<nav class="navbar navbar-dark bg-dark navbar-expand-lg menu">
			<a class="navbar-brand" href="index.php"><img class="img-fluid" src="<?=$url?>img/Logo.PNG"/></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
 		 	<a class="nav-link" style="color:white;padding-right: 2.5rem;padding-left: 2.5rem;" href="<?=$url?>index.php">Home</a>
			<a class="nav-link" style="color:white;padding-right: 2.5rem;padding-left: 2.5rem;"  href="<?=$url?>buscar.php">Buscar</a>
			<a class="nav-link" style="color:white;padding-right: 2.5rem;padding-left: 2.5rem;"  href="<?=$url?>contacto.php">Contacto</a>
			<a class="nav-link" style="color:white;padding-right: 2.5rem;padding-left: 2.5rem;"  href="<?=$url?>info.php">Deberías saber...</a>
            <?php if($_SESSION['user']==0){?>
            <a class="nav-link" style="color:lightblue; padding-left:12rem" href="<?=$url?>register.php">¿Aún no tienes cuenta?</a>
            <?php } ?>
    </div>
	</div>
	</nav>
<div class="row">
<div class="col-md-12"><br></div>
</div>