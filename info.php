<?php
session_start();
include 'conexion.php';
?>
<!DOCTYPE html>
<html>
<head>
<title>Informacion</title>
<style type="text/css">
.jumbotron{
    margin-left:100px;
}
</style>
 <?php
 include 'header.php';
 ?>
 </head>
  <body>
  <div class="row">
     <div class="jumbotron col-md-10">
          <div class="col-md-15">
            <h1 class="h1 text-center text-primary">Todo lo que hay que saber antes de llevarlo al hogar</h1><br/>
          </div>
          <hr>
          <div>
            <p class="lead">La decisión de incluir un nuevo integrante en la familia no debe ser tomada a la ligera.
             Cuáles son las responsabilidades que hay que asumir, y cómo es el proceso de adaptación de una mascota, 
            desde un cachorro hasta un perro adulto que pudo haber sufrido maltratos por parte de dueños anteriores</p>
            <hr>
            <p>Detrás de ese gesto, hay responsabilidades que no pueden dejar de cumplirse una vez tomada la decisión. 
            Encargarse de que tengan una buena alimentación, hagan ejercicio diario, sean estimulados a través del juego y
             reciban la atención periódica de un veterinario son algunos de los deberes a cumplir.
            </p>
            <p>
            Las protectoras tienen en cuenta de manera excluyente quiénes son los adoptantes. 
            Por lo general el procedimiento incluye una entrevista previa en la cual se conoce al interesado y 
            posteriormente, salvo excepciones en las cuáles el interés no sea genuino. Una vez que el perro está en 
            el hogar, se realiza un seguimiento que puede incluir visitas al domicilio para controlar el estado del 
            animal. Sin embargo, existen algunos casos en que es necesario recuperarlo para evitar futuros problemas.
            </p>
            <p>
            Hay que saber que el comportamiento dependerá en gran medida del ambiente que lo rodea desde su
             gestación y período de aprendizaje, hasta el tipo de estimulación que reciba.
            </p>
          </div>
          <div class="jumbotron col-md-15">
          <div class="col-md-15">
            <h1 class="h1 text-center text-primary">Leyes importantes</h1><br/>
          </div>
          <dl class="row">
            <dt class="col-sm-2">Vacunas</dt>
            <dd class="col-sm-10">Desde el primer mes de vida, un animal tiene que ponerse la primera vacuna.
             Luego el veterinario te dará los preceptos a seguir en las vacunas de los 3 meses, 
             los 6 meses, anuales y algunas especiales.</dd>
        </dl>
        <dl class="row">
            <dt class="col-sm-2">Microchip</dt>
            <dd class="col-sm-10">Con la primera vacuna, aproximadamente, vendría la inserción del microchip. 
            Es un localizador que te será muy útil si el animal se pierde. Permitirá que las autoridades sepan 
            de su existencia. No es tan solo algo legal y obligatorio, sino que además te servirá de protección 
            tanto para ti como para tu mascota.</dd>
        </dl>
        <dl class="row">
            <dt class="col-sm-2">Cartilla</dt>
            <dd class="col-sm-10">Esta será como una identificación de tu mascota. En ella el veterinario 
            rellenará las vacunas que tiene puestas, así como los datos del animal tales como peso, estatura, 
            salud y nombre. También irá aludido tu nombre como único dueño del animal.</dd>
        </dl>
        <dl class="row">
            <dt class="col-sm-2">Pasaporte</dt>
            <dd class="col-sm-10">En caso de que viajes y estés pensando hacerlo con tu nuevo amigo, 
            deberás hacerle un pasaporte que te exigirán las autoridades aeroportarias.</dd>
        </dl>
        <dl class="row">
            <dt class="col-sm-2">Leyes básicas</dt>
            <dd class="col-sm-10">Entre las que se encuentra el uso del bozal, la tenencia de mascotas y la recogida
            de sus excrementos.</dd>
        </dl>
        </div>
        </div>
  </div>
 </body>
 </html>