<?php
$url="http://".$_SERVER['SERVER_NAME']."/meetpet/"; 
$pass="";
$nombre="";
$id="";
$merror="";
$cont="";
$text="";
$nombre="";
$habilitar="";
if(isset ($_REQUEST['dog'])){
$habilitar="WHERE Habilitado='1'";
}
if(!isset($_REQUEST['nomb'])){
$_REQUEST['nomb']="";
}
if(isset($_REQUEST['borrar'])){
  $id=$_REQUEST['borrar'];
  $borrado = $conexion->query('DELETE  FROM perro WHERE IDperro="'.$id.'"');
}
if(isset($_REQUEST['habilitado'])){
  $habilitar = $conexion->query('UPDATE perro SET Habilitado=1 WHERE IDperro='.$_REQUEST['habilitado']);
  $merror="Se ha habilitado correctamente";
  header ('Location: admin.php?dog=true&validar=true');
}
if(isset($_REQUEST['nhabilitado'])){
  $habilitar = $conexion->query('DELETE FROM perro WHERE IDperro='.$_REQUEST['nhabilitado']);
  $merror="Se ha borrado este registro";
  header ('Location: admin.php?dog=true&validar=true');
}
if(isset($_REQUEST['añadir'])){
  header('Location : altaPerro.php');
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Admin</title>
<style type="text/css">
.hola{
    margin-left:400px;
    margin-right:400px;
}
</style>
    <link rel="stylesheet" type="text/css" media="screen" href="./css/bootstrap.css"/>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="<?=$url?>js/bootstrap.min.js"></script>
    <script src="<?=$url?>js/main.js"></script>
 </head>
 <body>
 <div class="page">
 <br/>
 <div class="row">
 <div class="col-md-1"></div>
  <div class="col">
  <?php
  if(!isset($_REQUEST['validar'])){?>
  <h2 class="text-center">Todos Los Perros</h2>
  <a class="btn btn-success" href="<?=$url?>altaPerro.php?go=true&admin=si" role="button">Añadir Perro</a>
  <a class="btn btn-warning" href="<?=$url?>mPerro.php?modificar=<?=$_REQUEST['id']?>" role="button">Modificar <?=$_REQUEST['nomb']?></a>
  <a class="btn btn-danger" href="<?=$url?>admin.php?dog=true&&borrar=<?=$_REQUEST['id']?>" role="button">Eliminar <?=$_REQUEST['nomb']?></a>
  <a class="btn btn-warning float-right" href="<?=$url?>admin.php?dog=true&&validar=true" role="button">Validar Perros</a>
  <?php
  }else{?>
  <h2 class="text-center">Validar perros</h2>
  <a class="btn btn-success" href="<?=$url?>admin.php?dog=true" role="button">Todos los perros</a>
  <?php
  $habilitar="WHERE Habilitado='0'";
  }
  ?>
    <table class="table">
    <thead>
      <tr>
        <th scope="col">ID</th>
        <th scope="col-7">Nombre</th>
        <th scope="col">Raza</th>
        <th scope="col-7">Tamaño</th>
        <th scope="col">Sexo</th>
        <th scope="col-7">Edad</th>
        <th scope="col">Reservado</th>
        <th scope="col-7">IDperrera</th>
        <th scope="col">Img</th>
        <th scope="col-7">Habilitado</th>
        <th scope="col">Contacto</th>

      </tr>
    </thead>
    <tbody>
    <?php
      $consulta = $conexion->query('SELECT * FROM perro '.$habilitar);
      if($consulta->num_rows!=0) {
        while($dis = $consulta->fetch_assoc()){
          $nombre=$dis['Nombre'];
          $idperro=$dis['IDperro'];
                ?>
                  <tr>
                    <td><?= $dis['IDperro']?></td>
                   <td><a href="admin.php?dog=true&nomb=<?=$nombre?>&id=<?=$idperro?>"><?= $dis['Nombre']?></a></td>
                    <td><?= $dis['Raza']?></td>
                    <td><?= $dis['Tamaño']?></td>
                    <td><?= $dis['Sexo']?></td>
                    <td><?= $dis['Edad']?></td>
                    <td><?= $dis['Reservado']?></td>
                    <td><?= $dis['IDperrera']?></td>
                    <td><img src="./img/<?= $dis['Img']?>" style="width:200px"/></td>
                    <td><?= $dis['Habilitado']?></td>
                    <td><?= $dis['Contacto']?></td>
                    <?php  if(isset($_REQUEST['validar'])){?>
                    <td><a class="btn btn-primary" href="<?=$url?>admin.php?dog=true&habilitado=<?= $dis['IDperro']?>" role="button">Habilitar </a></td>
                    <td><a class="btn btn-danger" href="<?=$url?>admin.php?dog=true&nhabilitado=<?= $dis['IDperro']?>" role="button">Borrar </a></td>
                    </tbody>
                    <?php }?>
                  </tr>
              <?php
        }
    }else{?>
      <td>No hay ningun registro</td>
    <?php } ?>
    </table>
    </div>
    <div class="col-md-1"></div>
    </div>
    </div>
 </body>
 </html>