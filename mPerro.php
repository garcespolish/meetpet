<?php
include 'conexion.php';
$url="http://".$_SERVER['SERVER_NAME']."/meetpet/"; 

if(isset($_POST['enviar'])){
    $mensaje="Perro Modificado";
    $nombre=(isset($_POST['nombre']) ? $_POST['nombre']:'');
    $raza=(isset($_POST['raza']) ? $_POST['raza']:'');
    $tamaño=(isset($_POST['tamaño']) ? $_POST['tamaño']:'');
    $sexo=(isset($_POST['sexo']) ? $_POST['sexo']:'');
    $edad=(isset($_POST['edad']) ? $_POST['edad']:'');
    $consulta = $conexion->query('UPDATE perro Set Nombre="'.$nombre.'", Raza="'.$raza.'", Tamaño= "'.$tamaño.'", Sexo = "'.$sexo.'", Edad = "'.$edad.'"WHERE IDperro='.$_REQUEST['modificar']);
   
}
if(!isset($_REQUEST['modificar'])){
  header('Location:admin.php?dog=true');
}
else{
    $consulta = $conexion->query('SELECT * FROM perro WHERE IDperro='.$_REQUEST['modificar']);
    if($consulta->num_rows!=0) {
        while($dis = $consulta->fetch_assoc()){
?>
<head>
<title>Admin  </title>
<style type="text/css">
</style>
    <link rel="stylesheet" type="text/css" media="screen" href="./css/bootstrap.css"/>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="<?=$url?>js/bootstrap.min.js"></script>
    <script src="<?=$url?>js/main.js"></script>
 </head>
 <body>
<form method="post" action="<?=$url?>mPerro.php?modificar=<?=$_REQUEST['modificar']?>" enctype="multipart/form-data">
          
        <div class="form-row">
            <div class="col form-group">
                <label for="exampleInputPassword1">Nombre del Perro</label>
                <input type="text" value=<?=$dis['Nombre']?> class="form-control" name="nombre">
            </div>
            <div class="col form-group">
                <label for="exampleInputEmail1">Raza</label>
                <input type="text" value=<?=$dis['Raza']?> class="form-control" name="raza">
            </div>
        </div>
       
        <div class="form-row">
        
            <div class="col form-group">
                <label for="exampleInputPassword1">Tamaño</label>
                <select class="custom-select mr-sm-2" name="tamaño">
                <?php if($dis['Tamaño']=="Pequeño"){ ?>
                        <option selected value="Pequeño">Pequeño</option>
                        <option value="Mediano">Mediano</option>
                        <option value="Grande">Grande</option>
                <?php } ?>
                <?php if($dis['Tamaño']=="Mediano"){ ?>
                        <option selected value="Mediano">Mediano</option>
                        <option value="Grande">Grande</option>
                        <option value="Pequeño">Pequeño</option>
                <?php } ?>
                <?php if($dis['Tamaño']=="Grande"){ ?>
                        <option selected value="Grande">Grande</option>
                        <option value="Mediano">Mediano</option>
                        <option value="Pequeño">Pequeño</option>
                <?php } ?>
                </select>
            </div>
            <div class="col form-group">
                <label for="exampleInputPassword1">Sexo</label>
                <select class="custom-select mr-sm-2" name="sexo">
                <?php if($dis['Sexo']=="Macho"){ ?>
                        <option selected value="Macho">Macho</option>
                        <option value="Hembra">Hembra</option>
                <?php } ?>
                <?php if($dis['Sexo']=="Hembra"){ ?>
                        <option selected value="Hembra">Hembra</option>
                        <option value="Macho">Macho</option>

                <?php } ?>
                </select>
            </div>
        </div>
        
        <div class="form-row">
        <div class="col-md-5">
            <div class=" form-group">
                <label for="exampleInputPassword1">Edad</label>
                <input type="text" value=<?=$dis['Edad']?> class="form-control" name="edad">
            </div>
            
            </div>
        </div>  
        <div class="form-row">
           
        </div>
        <div class="row">
        <div class="col-md-6">
        </div>
        <div class="col-md-6">
            <button type="submit" class="btn btn-primary" name="enviar">Enviar</button>
        </div>
       </div>
        </form>
<?php }}} ?>
<a href="admin.php?dog=true" class="btn btn-danger">Volver</a>
<p><?php if(isset($mensaje)){echo $mensaje;}?></p>
</body>