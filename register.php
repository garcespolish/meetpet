<?php
session_start();
include 'conexion.php';
$user="";
$psw="";
$email="";
$tlf="";
$merror="";
$mensaje="";
if(isset($_POST['enviar'])){
  $user = $_POST["user"];
  $psw = $_POST["psw"];
  $email = $_POST["email"];
  $tlf = $_POST["tlf"];
  if($user=="" || $email=="" || $psw==""){
    $merror="No se ha introducido un campo obligatorio";
  }else{
    $resultado = $conexion->query('SELECT * FROM usuario WHERE nombre="'.$user.'"');
    if($resultado->num_rows!=0) {
      $merror="Ya hay un usuario con este nombre";
       }else{
        $resultado=$conexion->query('INSERT INTO usuario (Nombre, Password, Email, Telefono, Admin) VALUES ("'.$user.'","'.$psw.'","'.$email.'","'.$tlf.'","2")');
        if(isset($_SESSION['admin'])){
          $mensaje="";
        }else{
          $mensaje="Se ha creado el usuario correctamente";
        }
       }
      }
      }
?>
<!DOCTYPE html>
<html>
<head>
<title>Registro</title>
<style type="text/css">
    h2{
      margin-left:-20px;
    }
    .foto{
      margin-top:30px; 
      margin-left:90px;
    }
</style>
<link rel=StyleSheet href="" type="text/css" media=screen>
 <?php
 include 'header.php';
 ?>
  </head>
  <body>
<div class="row">
  </div>
  <div class="row">
    <div class="col-md-1"></div>
    <?php 
    if($mensaje==""){

      if(isset($_REQUEST['admin'])){?>
        <form class="col-md-4" method="post" action="<?=$url?>register.php?admin=si">
    <?php
      }else{?>
    <form class="col-md-4" method="post" action="<?=$url?>register.php">
    <?php }
    ?>
    <form class="col-md-4" method="post" action="<?=$url?>register.php">
    <h2 class="text-primary">Registro:</h2><br/>
    <div class="form-group">
    <div class="form-group">
      <label for="exampleInputPassword1">Nombre de Usuario*</label>
      <input type="text" class="form-control" name="user" aria-describedby="nombHelp" >
      <small id="nombHelp" class="form-text text-muted">El nombre con el que iniciará sesion y nos referiremos a usted</small>
    </div>
      <label for="exampleInputEmail1">Direccion de email*</label>
      <input type="email" class="form-control" name="email" aria-describedby="emailHelp">
      <small id="emailHelp" class="form-text text-muted">No enviaremos spam ni nada relacionado</small>
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Contraseña*</label>
      <input type="password" class="form-control" name="psw">
   </div>
   <div class="form-group">
      <label for="exampleInputPassword1">Numero de Telefono</label>
      <input type="text" class="form-control" name="tlf" aria-describedby="tlfHelp" >
      <small id="tlfHelp" class="form-text text-muted">Solo por si hay algún problema y se require una comunicación más directa</small>
    </div>
    <h3 class="text-danger"><?=$merror?><br/></h3>
    <?php 
  if(isset($_REQUEST['admin'])){
    $_SESSION['loged']="si";
    $_SESSION['admin']="si";
    ?>
    <a class="btn btn-danger" href="admin.php?user=true" role="button">Volver</a>

    <?php
    }
    ?> 


    <button type="submit" class="btn btn-primary" name="enviar">Registrarse</button>
 
  </form>
  <?php
  }else{ ?>
  <div class="col-md-4">
  <h2 class="text-success"><?=$mensaje?></h2>
  <p>Ya puedes acceder con tu nuevo usuario desde la página principal!!</p>
  </div>
    <?php } ?>
  <div class="col-md-1"></div>
  <div class="col-md-5">
  <br/><br/>
  El registrarse permite al usuario contactar con los administradores y dar de alta perros.<br/>
  Esto significa que si por motivos personales no le es posible mantener un perro, no puede ocuparse de el 
  u otras razones, desde aquí sera capaz de de registrarlo para que otros usuarios que busquen perros lo puedan
  encontrar.
  <br/><br/>
  <img class="foto mg-fluid rounded" src="<?=$url?>img/perromano.png"/>
  </div>
</div>
 </body>
 </html>