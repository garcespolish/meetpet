<?php
session_start();
include 'conexion.php';
$merror="";
$texto="";
if (isset($_POST["enviar"])) {
    $texto=$_POST["text"];
    if(empty($texto)){
        $merror="No se ha introducido ningún texto";
}
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Contacto</title>
<style type="text/css">
    .formu{
     padding-left:40px;
    }
    h1, .text-success{
    padding-left:40px;
    }
    .card{
        margin-right:150px;
        margin-top:20px;
        float:right;
    }
</style>
<link rel=StyleSheet href="" type="text/css" media=screen>
 <?php
 include 'header.php';
 ?>
 </head>
 <body>
 <div class="card" style="width: 14rem;">
  <img class="card-img-top" src="./img/mejorar.png" alt="Card image cap">
  <div class="card-body">
    <p class="card-text">Tu opinion es importante para nosotros, no dudes en enviarnos tus sugerencias o dudas</p>
  </div>
  </div>

 <?php if($_SESSION['user']==1 && (!isset($_POST["enviar"]) || empty($texto))){ ?>
    <form class="formu" method="post" action="contacto.php">
    <h3 class="text-primary">¿Tiene alguna duda, alguna sugerencia?</h3>
        <br/><br/>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Escriba aquí:</label>
            <textarea class="form-control col-md-3" name="text" rows="4"></textarea><br/>
            <h4 class="text-danger"><?php echo($merror)?></h4>
        </div>
        <button type="submit" name="enviar" class="btn btn-primary">Enviar</button>
    </form>
 <?php }elseif($_SESSION['user']==1 && isset($_POST["enviar"]) && !empty($texto)){ ?>
     <h3 class="text-success">Tu mensaje ha sido enviado con éxito, gracias por su aportacion.</h3><br/>
     <h4 class="text-success"><u>Se enviara una respuesta via correo.</u></h4>
     <?php
     $resultado=$conexion->query('INSERT INTO mensaje (Texto, Enviado, Recibido) VALUES ("'.$texto.'","'.$_SESSION['id'].'",1)');
     ?>
 <?php }elseif($_SESSION['user']==0){?>
 <h1 class="text-danger">Debes estár registrado para poder contactar con el servicio técnico :S</h1>
 <?php }?>
 </body>
 </html>