<?php
$url="http://".$_SERVER['SERVER_NAME']."/meetpet/";
session_start();
include 'conexion.php';
$pass="";
$nomb="";
$merror="";
$cont="";
$text="";
if(!isset($_SESSION['loged'])){
$_SESSION['loged']="no";
}
if(isset($_POST['enviar'])){
    $nomb = $_POST["nomb"];
    $pass = $_POST["pass"];
    $resultado = $conexion->query('SELECT * FROM usuario WHERE nombre="'.$nomb.'"');
    if($resultado->num_rows!=0){
        while($dis = $resultado->fetch_assoc()){
         if($pass!=$dis['Password']){
            $merror="Contraseña incorrecta";
         }else{
            if($dis['Admin']==1){
                $_SESSION['loged']="si";
                $_SESSION['id']=1;
            }else{
                $merror="Este usuario no es administrador!!";
            }
        }
        }
    }else{
    $merror="Fallo al iniciar sesion";
    }
}
if(isset($_REQUEST['log'])){
  session_destroy();
  header('Location:admin.php');
}

?>
<!DOCTYPE html>
<html>
<head>
<title>Admin</title>
<style type="text/css">
.menu{
    margin-left:200px;
    margin-right:200px;
    font-size:28px;
}
.cuerpo{
    margin-left:200px;
    margin-right:200px;
}

</style>
    <link rel="stylesheet" type="text/css" media="screen" href="./css/bootstrap.css"/>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="<?=$url?>js/bootstrap.min.js"></script>
    <script src="<?=$url?>js/main.js"></script>
 </head>
 <body>
 <br/><br/>
 <?php if($_SESSION['loged']=="no"){ ?>
 <div class="row">
 <div class="col-md-2"></div>
 <div class="jumbotron col -md-8">
  <h1 class="display-4 text-center">Registrarse Admin</h1><br/>
  <p class="lead text-center">A este panel solo podrán acceder los administradores</p>
  <hr class="my-4">
  <form class="form col-md-4" method="post" action="admin.php">
      <div class="form-group">
        <label>Nombre de Usuario</label>
        <input type="text" class="form-control" name="nomb" placeholder="Nombre">
      </div>
      <div class="form-group">
        <label>Contraseña</label>
        <input type="password" class="form-control" name="pass" placeholder="Contraseña">
      </div>
      <h3 class="text-danger"><?=$merror?></h3>
      <button type="submit" class="btn btn-primary" name="enviar">Enviar</button>
     </form>
</div>
<div class="col-md-2"></div>
</div>
<?php }else{ ?>
<h1 class="text-center text-warning">Panel de Administrador</h1>
<a class="btn btn-danger float-right" href="<?=$url?>admin.php?log=out" role="button">Cerrar Sesión</a><br/>
<nav class="navbar navbar-dark bg-dark navbar-expand-lg menu">
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
 		 	<a class="nav-link" style="color:white;padding-right: 4rem;padding-left: 11rem;" href="<?=$url?>admin.php?user=true">Usuarios</a>
			<a class="nav-link" style="color:white;padding-right: 4rem;padding-left: 4rem;"  href="<?=$url?>admin.php?dog=true">Perros</a>
			<a class="nav-link" style="color:white;padding-right: 4rem;padding-left: 4rem;"  href="<?=$url?>admin.php?msg=true">Mensajes</a>
    </div>
	</div>
	</nav>
    <br/>
<?php 
  if (isset($_REQUEST['user'])){
    include 'adminU.php';
  }elseif(isset($_REQUEST['dog'])){
    include 'adminD.php';
  }else{
    include 'adminM.php';

}
}
?>
 </body>
 </html>